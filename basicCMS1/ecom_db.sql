-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 10, 2017 at 02:27 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ecom_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `specifications` text NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `seller_id` int(11) NOT NULL,
  `category_id` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`,`price`, `seller_id`, `category_id`) VALUES
(1, 'Casual hoodie (Available color: black and maroon)', 'Novelty loose fit hoodies for women and stylish girls. Keep warm and soft cotton use, winter sweatshirts with zipper neck and kangaroo pocket', '',480, 1, 1),
(2, 'Basic Zippered Sweater Hooded Jackets(Color: Beige)', 'Brand new sweater hoodies for women. Zippered closure, so basic hooded jackets for everyday. Casual and loose fitted outerwear for school, brunch, picnic.', '', 500, 1, 1),
(3, 'Loose cotton Hoodie(Available color: Black, Gray)', 'Casual long sleeved hoodies for women. Loose fit, 100% cotton hooded tops for school, jogging, picnic, travel. Korean street fashion hoodies for stylish girls.', '', 550, 1, 1),
(4, 'Swallow embroidery crewneck sweatshirt', 'Cute swallow embroidery sweatshirts for women. Crew neck cotton tops are made of 100% cotton, dolman long sleeves, loose fit, colorblocked novelty sweatshirts.', '', 550, 1, 1),
(5, 'Mermaid Lace Dress(Color: Beige, Black, Red, Green)', 'Elegant lace dresses for women. Mermaid and 3/4 sleeved dresses for party, wedding, dinner. Korean style dresses, feminine see-thru lace dresses.', '', 400, 1, 2),
(6, 'Ruffled long-sleeved blouse(Color: White, Red, Blue)', 'Glamorous evening ruffled long-sleeved blouses', '', 589, 1, 2),
(7, 'Floral off-shoulder(Color: White, Black, Navy, Red)', 'Loose fit ruffled blouses for women. Floral patterned tops with banding at top, ladies are going to wear as off-shoulder top or neck line blouses.', '', 560, 1, 2),
(8, 'Pleated buttoned blouse(Color: Gray, White, Red, Black)', 'Womens pleated front-ribbon buttoned blouses.', '', 380, 1, 2),
(9, 'Wrap Linen Mini Skirt(Color: Beige, Orange, Black)', 'Cool summer linen shorts for women. Attached wrap mini skirt detailed pants. Solid skirts, Korean drama fashion, novelty clothing.', '', 380, 1, 3),
(10, 'Synthetic sued mini skirt (Color:Beige, Black, Navy)', 'Vintage and gorgeous synthetic suede mini skirts for women. Side zipper closure, high waist, girlish soft pencil mini skirts. Korean drama style casual clothes.', '', 350, 1, 3),
(11, 'Multi Colored Patchwork Suede Mini Skirts for women', 'Multi-colored patchwork mini skirts for women. Casual and cute girls clothing, button-front skirts are made of suede. A-line flared suede skirt by Korea, South.', '', 400, 1, 3),
(12, 'Light blue Vintage Denim Mini Skirt(Available in Dark blue)', 'Casual and vintage denim skirts for women. 100% cotton, back waistband, zip-fly denim clothing.  Korean drama style skirts, Korean fashion for stylish girls.', '', 650, 1, 3),
(13, 'D-logo piercing bucket hat', 'Stylish bucket hats for men and women. 100% cotton bucket hats featuring a seam stitched brim and a flat crown. Also D embroidery logo and piercing trim.', '', 400, 1, 4),
(14, 'Casual baseball cap', 'Brand new corduroy baseball caps for men and women. Casual and vintage style hats, adjustable back buckled, solid baseball cap for everyday.', '', 350, 1, 4),
(15, 'Side piercing baseball cap', 'Side piercing accented solid baseball caps for men and women. 100% cotton, made in Korea, strapback baseball caps for school, picnic, weekend, travel, club.', '', 380, 1, 4),
(16, 'Dopeness cotton baseball cap', 'Brand new Dopeness embroidery baseball caps for men and women. 100% cotton ball caps, unisex accessories, Kpop style baseball caps for stylish guys and girls.', '', 400, 1, 4),
(17, 'Strappy-faux leather suede-sandal boho shoes', 'Colorful strappy sandals for women. Lightweight and comfort shoes, casual colorful sandals for everyday, brunch, party, picnic, weekend. Korean fashion shoes.', '', 300, 1, 5),
(18, 'Boho braided sandal(Color: Beige, Black, Red, Navy)', 'Bohemian mood braided sandals for women and girls. Boho style faux leather shoes for summer. Back band detail shoes so comfortable, casual sandals.', '', 300, 1, 5),
(19, 'Mary Jane faux-suede sandal(Color:Black, Beige, Blue)', 'Synthetic suede ankle strap sandals for women. Simple and basic, cubic detailed shoes, comfort chunky heel sandals. Mary Jane style open toe heel shoes.', '', 580, 1, 5),
(20, 'Faux-leather string sandals shoes(Color:Beige, Black)', 'Synthetic leather string sandals for women. Simply casual shoes for everyday in summer and vacation. Korean trendy shoes, accented cubic.', '', 600, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `position` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `description`, `status`, `position`) VALUES
(1, 'Hoodies/Sweatshirt', 'Nice and cute hoodies.', 1, 1),
(2, 'Blouse and Dress', 'Blouse and dress trends.', 1, 2),
(3, 'Skirts', 'Cute skirts.', 1, 3),
(4, 'Hats and Caps', 'Cool hats and caps.', 1, 4),
(5, 'Sandals', 'Beautiful sandals.', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `name`, `prod_id`, `description`) VALUES
(1, 'Casual hoodie (Available color: black and maroon)', 1, ''),
(2, 'Basic Zippered Sweater Hooded Jackets(Color: Beige)', 2, ''),
(3, 'Loose cotton Hoodie(Available color: Black, Gray)', 3, ''),
(4, 'Swallow embroidery crewneck sweatshirt', 4, ''),
(5, 'Mermaid Lace Dress(Color: Beige, Black, Red, Green)', 5, ''),
(6, 'Ruffled long-sleeved blouse(Color: White, Red, Blue)', 6, ''),
(7, 'Floral off-shoulder(Color: White, Black, Navy, Red)', 7, ''),
(8, 'Pleated buttoned blouse(Color: Gray, White, Red, Black)', 8, ''),
(9, 'Wrap Linen Mini Skirt(Color: Beige, Orange, Black)', 9, ''),
(10, 'Synthetic sued mini skirt (Color:Beige, Black, Navy)', 10, ''),
(11, 'Multi Colored Patchwork Suede Mini Skirts for women', 11, ''),
(12, 'Light blue Vintage Denim Mini Skirt(Available in Dark blue)', 12, ''),
(13, 'D-logo piercing bucket hat', 13, ''),
(14, 'Casual baseball cap', 14, ''),
(15, 'Side piercing baseball cap', 15, ''),
(16, 'Dopeness cotton baseball cap', 16, ''),
(17, 'Strappy-faux leather suede-sandal boho shoes', 17, ''),
(18, 'Boho braided sandal(Color: Beige, Black, Red, Navy)', 18, ''),
(19, 'Mary Jane faux-suede sandal(Color:Black, Beige, Blue)', 19, ''),
(20, 'Faux-leather string sandals shoes(Color:Beige, Black)', 20, '');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE IF NOT EXISTS `sellers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `name`, `status`, `description`) VALUES
(1, 'Hwaiting', 1, 'Hwaiting Korean Apparels');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `birthday` varchar(10) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `users`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_cart`
--

CREATE TABLE IF NOT EXISTS `user_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_cart`
--

